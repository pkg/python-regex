# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the regex test runner tool."""

from __future__ import annotations

import dataclasses
import typing


if typing.TYPE_CHECKING:
    import logging
    import pathlib
    from typing import Final


VERSION: Final = "0.1.0"
"""The test tool version, semver-like."""


FEATURES: Final = {
    "temptest": VERSION,
    "pybuild": "0.1",
    "srcdir": "0.1",
}
"""The list of features supported by the test tool."""


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the test tool."""

    log: logging.Logger
    """The logger to send diagnostic, informational, and error messages to."""

    pybuild: pathlib.Path | None
    """The .pybuild directory to look for the built Python module in."""

    pypath: pathlib.Path | None
    """The built Python module parent directory to add to PYTHONPATH."""

    srcdir: pathlib.Path
    """The directory to look for the regex module source code."""

    verbose: bool
    """Verbose operation; display diagnostic output."""
